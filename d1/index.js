// console.log("Hellow World");

// What are conditional statememts?
// allows us to control the flow of our program
// it allows us to run a statement/instruction if the condition is met or run another separate instruction if otherwise

// if, else if and else statement

let numA = -1;

// if statement
	// excutes a statement if a specified condition is true

	if(numA<0){
		console.log("Yes! It is less than zero")
	};

	/*
	Syntax:

	if(condition){
		statement
	}
	*/
	//  the result of the expression added in the if's condition msut result to true, else, the statement inside if() will not run
	console.log(numA<0); // results to true -- the if statement will run 

	numA = 0;
	if(numA<0){
		console.log("Hello")
	}
	console.log(numA<0); //will return false

	let city = "New York";
	if (city === "New York"){
		console.log("Welcome to New York City!")
	};

	// else if clause
	/*
	executes a statement if previous conditions are false and if the specified condition is true
	the "else if" clause is optional and can be added to capture additional conditions to change the flow of a program
	*/

	let numH = 1;

	if(numA<0){
		console.log("Hello");
	} else if (numH>0){
		console.log("World");
	};

	// we were able to run the else if() statement after we evaluated that the if condition was failed

	// if the if() condition was passed and run, we will no longer evaluate the else if() and end the process

	numA = 1;

	if(numA>0){
		console.log("Hello");
	} else if (numH>0){
		console.log("World");
	};

	// else if() statement was no longer run because the if statement was able to run, the evaluation of the whole statement stps there

	city = "Tokyo"

	if(city === "New York"){
		console.log("Welcome to New York City!")
	} else if(city === "Tokyo"){
		console.log("Welcome to Tokyo, Japan!")
	};

	// since we failed the condition for the first if(), we went to else if() and checked and instead passed that condition

	// esle statement
	/*
	-executes a statement if all other conditions are false
	-the 'else' statement is optional and can be added to capture any other result to change the flow of a program
	*/

	if(numA<0){
		console.log("Hello");
	} else if(numH === 0){
		console.log("World");
	} else {
			console.log("Again");
	};

	/*
	SInce both the preceeding if and else if conditions are not met/failed, the else statement was run instead

	Else statements should only be added if there is a preceeding if condition

	Else statements by itself will not work, however, if statements will work even if there is no else statement
	*/

	// else{
	// 	console.log("Will not run without if")
	// } //else will not work without if()

	// else if(numH === 0){
	// 	console.log("World")
	// } else {
	// 	console.log("Again")
	// };

	// same goes for an else if, there should be preceeding if() first

	// if, else if and else statements with functions

	/*
	-most of the times we would like to use if, else if and else statements with functions to control the flow of our application
	*/

	let message = "No message.";
	console.log(message);

	function determineTyphoonIntensity(windSpeed){
		if(windSpeed < 30){
			return "not a typhoon yet";
		} else if(windSpeed<=61){
			return "Tropical depression detected";
		} else if(windSpeed >=62 && windSpeed <=88){
			return "Tropical storm detected"
		}else if(windSpeed >=89 || windSpeed <=117){
			return "Severe tropical storm detected"
		}else{
			return "Typhoon detected";
		}
	};
	// this returns the string to the variable "message" that invoked it
	message = determineTyphoonIntensity(110);
	console.log(message);

	if (message == "Severe tropical storm detected"){
		console.warn(message);
	};
	// console.warn is a good way to print warnings in our console that would help us developers act on some certain output with our code

	// function oddOrEvenChecker(num){
	// 		if(num % 3 === 0){
	// 		alert(num + "This number is even");
	// 	} 	else{
	// 		alert(num + "This number is odd");
	// 		}
	// 	};

	// 	oddOrEvenChecker(22);

	// function ageChecker(num){
	// 		if(num > 20){
	// 		alert(num + " is allowed to drink");
	// 		return false
	// 	}	else{
	// 		alert(num + " is not allowed to drink" )
	// 		return true
	// 		}
	// 	};

	// let isAllowedToDrink = ageChecker(30);
	// console.log(isAllowedToDrink);

	// Truthy and Falsy values

	/*
	In JS a "truthy" value is a value that is considrered true when encountered in a Boolean context
	values in JS are considered true unless defined otherwise
	Falsy values/exceptions for truth:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. Nan
	*/

	// Truthy Examples
	/*
	If the result of an expression in a condition results to a truthy value, the condition returns and the corresponding statements are executed
	Expressions are any unit of code that can be evaluated to a value
	*/

	if(true){
		console.log("Truthy")
	}

	if(34){
		console.log("Truthy")
	}


	if([1,2,3]){
		console.log("Truthy")
	}

	// Falsy examples

	if(false){
		console.log("Falsy")
	}
	if(0){
		console.log("Falsy")
	}
	if(undefined){
		console.log("Falsy")
	}

	// Conditional(Ternary) Operator
	/*
	The conditional (Ternary) operator takes in three operands:
		1.condition
		2.expression to execute if the condition is truthy
		2.expression to execute if the condition is falsy


		- can be used as alternative to an "if else" statement
		- ternary operators have an implicit "return" statement meaning without the return keyword the resulting expressions can be stored in a variable
		-Syntax 
			(expression) ? ifTrue : ifFalse
	*/

	// SIngle statement execution

	let ternaryResult = (1 < 18) ? true : false;
	console.log("The result of ternary operator: " + ternaryResult);
	console.log(ternaryResult);

	// Multiple statement execution 
	/*
	a functions maybe defined then used in a ternary operator
	*/

	let name;

	function isOfLegalAge(){
		name = "John";
		return "You are of the legal age limit"
	};

	function isUnderAge(){
		name = "Jane";
		return "You are under the age limit"
	};

	let age = parseInt(prompt("What is your age"));
	console.log(age);

	let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
	console.log("Result of ternary Operator in functions: " + legalAge + ' , ' + name);

	// Switch statement
		// the switch statement evaluates an expression and matches the expression's value to a case clause
		// .toLowerCase Function will change the input received from the prompt into all lowercase letters ensuring a match with the switch case
		// the expression is the infromation used to match the value provided in the switch cases
		// variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
		// break statement is used to terminate the current loop once a match has been found

	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

	switch (day) {
		case 'monday' : 
		console.log("the color of the day is red");
			break;
		case 'tuesday' : 
		console.log("the color of the day is orange");
			break;
		case 'wednesday' : 
		console.log("the color of the day is yellow");
			break;
		case 'thursday' : 
		console.log("the color of the day is green");
			break;
			case 'friday' : 
		console.log("the color of the day is blue");
			break;
		case 'saturday' : 
		console.log("the color of the day is indigo");
			break;
		case 'sunday' : 
		console.log("the color of the day is violet");
			break;
		default:
		console.log("Please input a valid day");
			break;
	};

	// Mini actitiy

	function determineBear(bearNumber){
		let bear;
		switch(bearNumber){
			case 1:
				alert("Hi! I'm Amy!");
			break;
			case 2:
				alert("Hi! I'm Lulu!");
			break;
			case 3:
				alert("Hi! I'm Morgan!");
			break;
			default:
			bear = bearNumber + 'is out of bounds';
			break;
		}
		return bear;
	};
	determineBear(2);


	// Try-Catch-Finally Statement
	/*
	- try catch statements are commonly used for error handling
	- they are used to specify a response whenever an error is received
	*/

	function showIntensityAlert(windSpeed){
		try {
			alerat(determineTyphoonIntensity(windSpeed))
			// error/err are commonly used variable names used by developers for storing errors
		}catch (error){
			console.log(typeof error);
			// the error.message is used to access the information relating to an error object
			console.warn(error.message);
		}finally{
			// continue execution of code regardless of success and failure of code execution in the 'try' block to handle/resolve erros
			alert("Intensity updates will show new alert")
		}
	}
	showIntensityAlert(56);




